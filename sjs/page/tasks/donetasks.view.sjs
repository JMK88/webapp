/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class DoneTasksView {

  constructor(task) {
    this.log = new Logger("DoneTasksView");
    this.controller = new TasksController();
    this.utils = new Utils();

    this.log.debug("Creating done task view");
    let ref = this;
    this.utils.loadTemplate("#app", "tasks/done-task-page.html", function() {
      ref._buildPage(task);
      ref._hookSaveButtonListener(task);
    });
  }

  _buildPage(task) {
    $("#task-name").text(task.name);
    let ref = this;
    $("input[name=user-search]").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#user-container .user-item").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
      });
    });
    $("input[name=bonus-search]").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#bonus-container .bonus-item").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
      });
    });
    this.controller.fetchUsers(function(users) {
      users.forEach(function(user) {
        if("MASTER" != user.role)
          $("#user-container").append(
            "<div class=\"user-item\">" +
            "<input class=\"m-3\" type=\"checkbox\" name=\"user\" value=\"" + user.id + "\"" +
            " id=\"user-" + user.id + "\">" +
            "<label for=\"user-" + user.id + "\">" + user.forename + " " +
            user.name + " (" + user.username + ")</label>" +
            "</div>"
          );
      });
    });
    this.controller.fetchBoni(function(boni) {
      boni.forEach(function(bonus) {
        ref.log.debug("Bonus value: " + bonus.value);
        let bonusValue = "";
        if("RELATIVE" == bonus.type) {
          let percent = (parseFloat(bonus.value) - 1) * 100;
          bonusValue = (percent >= 0 ? "+ " : "- ") + Math.round(percent) + "%";
        } else if ("ABSOLUTE" == bonus.type) {
          let value = parseFloat(bonus.value);
          if(value >= 0) {
            bonusValue = "+ " + Math.abs(value) + "XP";
          } else {
            bonusValue = "- " + Math.abs(value) + "XP";
          }
        }
        $("#bonus-container").append(
          "<div class=\"bonus-item\">" +
          "<input class=\"m-3\" type=\"checkbox\" name=\"bonus\" value=\"" + bonus.id + "\"" +
          " id=\"bonus-" + bonus.id + "\">" +
          "<label for=\"bonus-" + bonus.id + "\">" + bonus.name +
          " (" + bonusValue + ")</label>" +
          "</div>"
        );
      });
    }, function() {});
  }

  _hookSaveButtonListener(task) {
    this.log.debug("Setting up the save button listener.");
    let ref = this;
    let button = $("#save");
    button.click(function(e) {
      e.preventDefault();
      ref.log.debug("Saving...");
      let asignees = [];
      $("#user-container input[type=checkbox]:checked").each(function() {
        asignees.push($(this).val());
      });
      let boni = [];
      $("#bonus-container input[type=checkbox]:checked").each(function() {
        boni.push($(this).val());
      });
      let archiveTask = $("#archive-task").is(":checked");
      button.attr("disabled", "");
      ref.utils.addSpinner(button, true);
      ref.controller.markAsDone(task.id, asignees, boni, archiveTask, function() {
        ref.utils.removeSpinner(button, true);
        new TasksView();
      }, function() {
        button.removeAttr("disabled");
        ref.utils.removeSpinner(button, true);
      });
    });
  }

}
