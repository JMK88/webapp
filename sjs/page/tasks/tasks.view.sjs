/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class TasksView {

  constructor() {
    this.log = new Logger("TasksView");
    this.utils = new Utils();
    this.controller = new TasksController();

    $("title").text("Aufgaben");
    history.pushState({page:0}, $("title").text(), "?p=tasks");
    let ref = this;
    let templates = [];
    templates.push({container: "#app", name: "tasks/main.html"});
    templates.push({container: "#modals", name: "tasks/create-task-modal.html"});
    this.utils.loadMultipleTemplates(templates, function() {
      ref._setupPage();
    });
  }

  _setupPage() {
    let ref = this;
    this.controller.fetchTasks(function(tasks) {
      tasks.forEach(function(task) {
        ref._loadTask(task);
      });
      $("input[name=search]").on("keyup", function() {
        ref.log.debug("Searching...");
        var value = $(this).val().toLowerCase();
        $(".task").filter(function() {
          $(this).toggle($(this).find(".task-title").text().toLowerCase().indexOf(value) > -1);
        });
      });
    }, function() {
      alert("Tasks could not be loaded.");
      new OverviewView();
    });
  }

  _loadTask(task) {
    let ref = this;
    this.utils.loadTemplate("#tasks-buffer", "tasks/task.html", function() {
      let taskHtml = $("#tasks-buffer .task:last-child");
      taskHtml.find(".task-title").text(task.name);
      taskHtml.find(".task-description").text(task.description);
      taskHtml.find(".xp").text(task.xp);
      taskHtml.attr("data-id", task.id);
      taskHtml.find("button.mark-done").click(function(e) {
        e.preventDefault();
        new DoneTasksView(task);
      });
      taskHtml.find("button.edit").click(function(e) {
        let modal = $("#create-task-modal");
        let button = modal.find("button.save");
        let taskName = modal.find("input[name=name]");
        taskName.val(task.name);
        modal.find("textarea[name=description]").val(task.description);
        let xp = modal.find("input[name=xp]");
        xp.val(task.xp);
        ref._hookTaskNameAndXpValidator(taskName, xp, button);
        button.attr("data-task", task.id);
        ref._hookSaveButtonListener(button);
        modal.modal('show');
      });
      taskHtml.appendTo("#tasks-container");
    });
  }

  _hookTaskNameAndXpValidator(name, xp, button) {
    let activateButton = function(name, xp) {
      if(name.val().trim() != "" && xp.val().trim() != "" && parseInt(xp.val().trim()) != 0) {
        button.removeAttr("disabled");
      } else {
        button.attr("disabled", "");
      }
    };
    name.on("keyup", function() {
      if(name.val().trim() == "") {
        name.addClass("is-invalid");
      } else {
        name.removeClass("is-invalid");
      }
      activateButton(name, xp);
    });
    xp.on("keyup", function() {
      if(xp.val().trim() == "" || parseInt(xp.val()) == 0) {
        xp.addClass("is-invalid");
      } else {
        xp.removeClass("is-invalid");
      }
      activateButton(name, xp);
    });
  }

  _hookSaveButtonListener(button) {
    let ref = this;
    let modal = $("#create-task-modal");
    let name = modal.find("input[name=name]");
    let description = modal.find("textarea[name=description]");
    let xp = modal.find("input[name=xp]");
    button.click(function(e) {
      ref.utils.addSpinner(button, true);
      ref.controller.editTask(button.attr("data-task"), name.val(), description.val(), xp.val(), function() {
        ref.utils.removeSpinner(button, true);
        let taskCard = $("#tasks-container .task[data-id=" + button.attr("data-task") + "]");
        taskCard.find(".task-title").text(name.val());
        taskCard.find(".task-description").text(description.val());
        taskCard.find(".xp").text(xp.val());
        name.val("");
        description.val("");
        xp.val("");
        modal.modal('hide');
      }, function() {
        ref.utils.removeSpinner(button, true);
        alert("Task not edited.");
      });
    });
  }

}
