/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class GameMasterContent {

  constructor() {
    this.log = new Logger("GameMasterContent");
    this.utils = new Utils();
    this.controller = new OverviewController();

    this.log.debug("Creating game master content.");

    let ref = this;
    let templates = [];
    templates.push({container: "#content", name: "overview/master/master-content.html"});
    templates.push({container: "#modals", name:"overview/master/master-content-modals.html"});
    this.utils.loadMultipleTemplates(templates, function() {
      ref.utils.loadTemplate("#create-task-modal-outer", "tasks/create-task-modal.html", function() {
        new TeamsCard("#manage-teams-card", "#create-team-modal-container");
        ref._setupPage();
      });
    });
  }

  _setupPage() {
    this._hookTaskValidator();
    this._hookTaskButtonListener();
    this._hookTasksOverviewButtonListener();
    this._hookBonusValidator();
    this._hookBonusSaveButtonListener();
    new RankingContent("#rankings");
  }

  _hookTaskValidator() {
    this.log.debug("Adding the task input validators.");
    let ref = this;

    let nameValid = false;
    let xpValid = false;

    $("#create-task-modal input[name=name]").on("keyup", function() {
      ref.log.debug("Validating task name...");
      let input = $("#create-task-modal input[name=name]");
      let button = $("#create-task-modal button.save");
      if(input.val().trim() === "") {
        input.removeClass("is-valid");
        input.addClass("is-invalid");
        button.attr("disabled", "");
        nameValid = false;
      } else {
        input.removeClass("is-invalid");
        input.addClass("is-valid");
        nameValid = true;
        if(nameValid && xpValid)
          button.removeAttr("disabled");
      }
    });

    $("#create-task-modal input[name=xp]").on("keyup", function() {
      ref.log.debug("Validating xp input...");
      let input = $("#create-task-modal input[name=xp]");
      let button = $("#create-task-modal button.save");
      if(input.val().trim() === "" || parseInt(input.val()) == 0) {
        input.removeClass("is-valid");
        input.addClass("is-invalid");
        button.attr("disabled", "");
        xpValid = false;
      } else {
        input.removeClass("is-invalid");
        input.addClass("is-valid");
        xpValid = true;
        if(nameValid && xpValid)
          button.removeAttr("disabled");
      }
    });
  }

  _hookTaskButtonListener() {
    this.log.debug("Adding save task button listener.");

    let ref = this;
    $("#create-task-modal button.save").click(function(e) {
      e.preventDefault();
      ref.controller.whoami(function(user) {
        let name = $("#create-task-modal input[name=name]");
        let description = $("#create-task-modal textarea[name=description]");
        let xp = $("#create-task-modal input[name=xp]");
        let button = $("#create-task-modal button.save");

        ref.utils.addSpinner(button, true);
        button.attr("disabled", "");

        ref.controller.addTask(user.principal, name.val(), description.val(), parseInt(xp.val()), function() {
          $("#create-task-modal").modal('hide');
          name.val("");
          name.removeClass("is-valid");
          description.val("");
          description.removeClass("is-valid");
          xp.val("");
          xp.removeClass("is-valid");
          ref.utils.removeSpinner(button, true);
        }, function() {
          ref.utils.removeSpinner(button, true);
          button.removeAttr("disabled");
        });
      });
    });
  }

  _hookTasksOverviewButtonListener() {
    $("#tasks-overview-button").click(function(e) {
      e.preventDefault();
      new TasksView();
    });
  }

  _hookBonusValidator() {
    this.log.debug("Adding the bonus input validators.");
    let ref = this;

    let name = $("#create-bonus-modal input[name=name]");
    let value = $("#create-bonus-modal input[name=value]");
    let button = $("#create-bonus-modal button.save");

    name.on("keyup", function() {
      ref.log.debug("Validating bonus name...");
      if(ref._isBonusNameValid(name)) {
        name.removeClass("is-invalid");
        name.addClass("is-valid");
        if(ref._isBonusValueValid(value))
          button.removeAttr("disabled");
      } else {
        button.attr("disabled", "");
        name.removeClass("is-valid");
        name.addClass("is-invalid");
      }
    });

    value.on("keyup", function() {
      ref.log.debug("Validating value input...");
      if(ref._isBonusValueValid(value)) {
        value.removeClass("is-invalid");
        value.addClass("is-valid");
        if(ref._isBonusNameValid(name))
          button.removeAttr("disabled");
      } else {
        button.attr("disabled", "");
        value.removeClass("is-valid");
        value.addClass("is-invalid");
      }
    });
  }

  _isBonusNameValid(name) {
    return name.val().trim() != "";
  }

  _isBonusValueValid(value) {
    return !(value.val().trim() === "" || isNaN(value.val().replace(",", "."))
        || parseFloat(value.val()) == 0 || value.val().includes(" "));
  }

  _hookBonusSaveButtonListener() {
    this.log.debug("Adding save bonus button listener.");

    let ref = this;
    $("#create-bonus-modal button.save").click(function(e) {
      e.preventDefault();
      ref.controller.whoami(function(user) {
        let name = $("#create-bonus-modal input[name=name]");
        let type = $("#create-bonus-modal select[name=type]");
        let value = $("#create-bonus-modal input[name=value]");
        let button = $("#create-bonus-modal button.save");

        ref.utils.addSpinner(button, true);
        button.attr("disabled", "");

        let replacedValue = value.val().replace(",", ".");

        ref.controller.addBonus(user.principal, name.val(), type.val(), parseFloat(replacedValue), function() {
          $("#create-bonus-modal").modal('hide');
          name.val("");
          name.removeClass("is-valid");
          value.val("");
          value.removeClass("is-valid");
          ref.utils.removeSpinner(button, true);
        }, function() {
          ref.utils.removeSpinner(button, true);
          button.removeAttr("disabled");
        });
      });
    });
  }

}
