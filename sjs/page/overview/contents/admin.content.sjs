/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class AdminContent {

  constructor() {
    this.log = new Logger("AdminOverviewContent");
    this.utils = new Utils();
    this.controller = new OverviewController();

    this.log.debug("Creating admin content.");
    let ref = this;
    let templates = [];
    templates.push({container: "#content", name: "overview/admin/admin-content.html"});
    templates.push({container: "#modals", name: "overview/admin/admin-content-modals.html"});
    this.utils.loadMultipleTemplates(templates, function() {
      ref.teamsCard = new TeamsCard("#manage-teams-card", "#manage-team-modal-container");
      ref._setupPage(ref);
    });
  }

  _setupPage(ref) {
    ref.controller.fetchPrincipals(function(principals) {
      principals.forEach(function(principal) {
        ref._loadPrincipal(principal);
      });
    }, function() {
      ref.log.warn("Principals not fetched and not added to the view.");
    });
    ref._hookButtonListeners(ref);
  }

  _hookButtonListeners(ref) {
    ref.log.debug("Setting up listeners and validators.");
    ref._hookPrincipalNameValidator(ref);
    ref._hookSavePrincipalListener(ref);
    ref._hookUserDataValidator(ref);
    ref._hookUserDataSaveListener(ref);
  }

  _hookPrincipalNameValidator(ref) {
    ref.log.debug("Adding the principal name validator.");
    $("#create-principal-modal input[name=principal]").on("keyup", function() {
      ref.log.debug("Validating principal name...");
      let input = $("#create-principal-modal input[name=principal]");
      let button = $("#create-principal-modal button.save");
      if(input.val().trim() === "") {
        button.attr("disabled", "");
        input.removeClass("is-valid");
        input.addClass("is-invalid");
      } else {
        input.removeClass("is-invalid");
        input.addClass("is-valid");
        button.removeAttr("disabled");
      }
    });
  }

  _hookSavePrincipalListener(ref) {
    ref.log.debug("Adding the principal save button listener.");
    $("#create-principal-modal button.save").click(function(e) {
      e.preventDefault();
      let button = $("#create-principal-modal button.save");
      let input = $("#create-principal-modal input[name=principal]");
      ref.utils.addSpinner(button, true);
      button.attr("disabled", "");
      ref.controller.addPrincipal(input.val(),
        function(principal) {
          ref.utils.removeSpinner(button, true);
          input.val("");
          input.removeClass("is-valid");
          $("#create-principal-modal").modal('hide');
          ref._loadPrincipal(principal);
        }, function() {
          ref.log.warn("Principal not created.");
          button.removeAttr("disabled");
        });
    });
  }

  _loadPrincipal(principal) {
    $("select.principal-selection").append(
      "<option value=\"" + principal.id + "\">" + principal.name + "</option>"
    );
  }

  _hookUserDataValidator(ref) {
    let username = $("#create-user-modal input[name=username]");
    let principal = $("#create-user-modal select[name=principal]");
    let role = $("#create-user-modal select[name=role]");
    let password = $("#create-user-modal input[name=password]");
    let button = $("#create-user-modal button.save");

    username.on("keyup", function() {
      ref.log.debug("Validating username input...");
      if(username.val().trim() !== "") {
        username.removeClass("is-invalid");
        username.addClass("is-valid");
        button.removeAttr("disabled");
      } else {
        username.removeClass("is-valid");
        username.addClass("is-invalid");
        button.attr("disabled", "");
      }
    });

    password.change(function() {
      ref.log.debug("Validating password input...");
      if(password.val().trim() !== "") {
        password.removeClass("is-invalid");
        password.addClass("is-valid");
        button.removeAttr("disabled");
      } else {
        password.removeClass("is-valid");
        password.addClass("is-invalid");
        button.attr("disabled", "");
      }
    });

    role.change(function() {
      if(role.val() === "ADMIN") {
        principal.attr("disabled", "");
        let principals = principal.html();
        principal.html(
          "<option value=\"null\"></option>" + principals
        );
      } else {
        principal.find("option[value=null]").remove();
        principal.removeAttr("disabled");
      }
    });
  }

  _hookUserDataSaveListener(ref) {
    let username = $("#create-user-modal input[name=username]");
    let principal = $("#create-user-modal select[name=principal]");
    let forename = $("#create-user-modal input[name=forename]");
    let name = $("#create-user-modal input[name=name]");
    let password = $("#create-user-modal input[name=password]");
    let role = $("#create-user-modal select[name=role]");
    let button = $("#create-user-modal button.save");

    button.click(function(e) {
      e.preventDefault();
      ref.utils.addSpinner(button, true);
      button.attr("disabled", "");
      let principalVal = null;
      if(principal.val() !== "null") {
        principalVal = principal.val();
      }
      ref.controller.addUser(principalVal, username.val(), password.val(), forename.val(),
        name.val(), role.val(), function(user) {
          ref.utils.removeSpinner(button, true);
          button.removeAttr("disabled");
          username.val("");
          username.removeClass("is-valid");
          password.val("");
          password.removeClass("is-valid");
          forename.val("");
          name.val("");
          $("#create-user-modal").modal('hide');
        }, function() {
          ref.utils.removeSpinner(button, true);
          button.removeAttr("disabled");
        });
    });
  }

}
