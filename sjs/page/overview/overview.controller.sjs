/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class OverviewController {

  constructor() {
    this.lsm = new LocalStorageManager();
    this.utils = new Utils();
    this.userService = new UserService();
    this.principalService = new PrincipalService();
    this.teamService = new TeamService();
    this.log = new Logger("OverviewController");
  }

  checkRights() {
    this.log.debug("Checking user rights.");
    if(!this.userService.checkSession()) {
      this.log.info("User not authenticated. Returning to login.");
      new LoginView();
      return false;
    }
    return true;
  }

  getRole(callback) {
    this.log.debug("Checking role");
    let ref = this;
    this.userService.whoami(function(user) {
      callback(user.role);
    }, function() {
      ref.log.error("No role fetched.")
    });
  }

  whoami(callback) {
    let ref = this;
    this.userService.whoami(function(user) {
      callback(user);
    }, function() {
      ref.log.warn("Logging out.");
      ref.userService.logout();
      new LoginView();
    });
  }

  fetchProfilePicture(userId, callback) {
    let ref = this;
    ref.userService.fetchProfilePicture(userId, callback, function() {
      // do nothing - dfault loaded by template
    });
  }

  fetchPrincipals(successCallback, errorCallback) {
    this.principalService.fetchPrincipals(successCallback, errorCallback);
  }

  addPrincipal(name, successCallback, errorCallback) {
    let header = this.utils.createAjaxHeader();
    let request = {name: name};
    let ref = this;

    $.ajax({
      url: ref.lsm.getServerUrl() + "/api/0.1/principal/create",
      method: "POST",
      datatype: "json",
      data: request,
      headers: header,
      success: function(data, status, xhr) {
        var response = JSON.parse(data);
        ref.log.info("Principal created: " + data);
        successCallback(response);
      },
      error: function(xhr, status, error) {
        errorCallback();
      }
    });
  }

  addUser(principal, username, password, forename, name, role, successCallback, errorCallback) {
    let header = this.utils.createAjaxHeader();
    let request = {
      username: username,
      forename: forename,
      password: password,
      name: name,
      role: role
    };
    let ref = this;

    if(null != principal) {
      request.principal = principal;
    }

    $.ajax({
        url: ref.lsm.getServerUrl() + "/api/0.1/users/create",
        method: "POST",
        datatype: "json",
        data: request,
        headers: header,
        success: function(data, status, xhr) {
          var response = JSON.parse(data);
          ref.log.info("User created.");
          successCallback(response);
        },
        error: function(xhr, status, error) {
          ref.log.warn("User not created.");
          errorCallback();
        }
      });
  }

  logout() {
    this.userService.logout();
    new LoginView();
  }

  addTask(principal, name, description, xp, successCallback, errorCallback) {
    let header = this.utils.createAjaxHeader();
    let request = {
      name: name,
      description: description,
      xp: xp
    };

    if(null != principal) {
      request.principal = principal;
    }

    let ref = this;
    $.ajax({
        url: ref.lsm.getServerUrl() + "/api/0.1/tasks/create",
        method: "POST",
        datatype: "json",
        data: request,
        headers: header,
        success: function(data, status, xhr) {
          ref.log.info("Task created.");
          ref.log.debug(data);
          var response = JSON.parse(data);
          successCallback(response);
        },
        error: function(xhr, status, error) {
          ref.log.error("Task not created.");
          errorCallback();
        }
      });
  }

  addBonus(principal, name, type, value, successCallback, errorCallback) {
    let header = this.utils.createAjaxHeader();
    let request = {
      name: name,
      type: type,
      value: value
    };

    if(null != principal) {
      request.principal = principal;
    }

    this.log.debug("Sending add bonus request: " + JSON.stringify(request));
    let ref = this;
    $.ajax({
        url: ref.lsm.getServerUrl() + "/api/0.1/bonus/create",
        method: "POST",
        datatype: "json",
        data: request,
        headers: header,
        success: function(data, status, xhr) {
          ref.log.info("bonus created.");
          ref.log.debug(data);
          var response = JSON.parse(data);
          successCallback(response);
        },
        error: function(xhr, status, error) {
          ref.log.error("bonus not created.");
          errorCallback();
        }
      });
  }

  fetchRankings(successCallback, errorCallback) {
    let header = this.utils.createAjaxHeader();
    let ref = this;
    this.teamService.fetchTeams(function(teams) {
      let result = [];
      let doneRequest = 0;
      teams.forEach(function(team) {
        let request = {
          team: team.id
        };
        $.ajax({
            url: ref.lsm.getServerUrl() + "/api/0.1/ranking",
            method: "GET",
            datatype: "json",
            data: request,
            headers: header,
            success: function(data, status, xhr) {
              ref.log.info("Ranking fetched.");
              ref.log.debug(data);
              var response = JSON.parse(data);
              result.push({team: team, ranking: response});
              if(++doneRequest == teams.length) {
                ref.log.info("Fetched rankings: " + JSON.stringify(result));
                successCallback(result);
              }
            },
            error: function(xhr, status, error) {
              ref.log.error("Ranking not fetched.");
              errorCallback();
            }
          });
      });
    }, function() {
      errorCallback();
    })
  }

  fetchUser(id, successCallback, errorCallback) {
    this.userService.fetchUser(id, successCallback, errorCallback);
  }

}
