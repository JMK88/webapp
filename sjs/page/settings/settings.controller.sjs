/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class SettingsController {

  constructor() {
    this.log = new Logger("SettingsController");
    this.userService = new UserService();
    this.utils = new Utils();
    this.lsm = new LocalStorageManager();
  }

  fetchUserData(successCallback, errorCallback) {
    this.userService.whoami(successCallback, errorCallback);
  }

  changeUserData(username, forename, lastname, successCallback, errorCallback) {
    let header = this.utils.createAjaxHeader();
    let request = {
      username: username,
      forename: forename,
      name: lastname
    };
    let ref = this;
    $.ajax({
      url: ref.lsm.getServerUrl() + "/api/0.1/users/change-user-data",
      method: "POST",
      datatype: "json",
      data: request,
      headers: header,
      success: function(data, status, xhr) {
        ref.log.info("User data changed.");
        successCallback();
      },
      error: function(xhr, status, error) {
        errorCallback();
      }
    });
  }

  changePassword(password, successCallback, errorCallback) {
    let request = {"new_password": password};
    let header = this.utils.createAjaxHeader();
    let ref = this;

    $.ajax({
        url: this.lsm.getServerUrl() + "/api/0.1/users/change-password",
        method: "POST",
        datatype: "json",
        data: request,
        headers: header,
        success: function(data, status, xhr) {
          ref.log.debug("Change password successful: " + data);
          successCallback();
        },
        error: function(xhr, status, error) {
          ref.log.error("Change password failed.");
          errorCallback();
        }
      });
  }

  changeProfilePicture(imageSrc, successCallback, errorCallback) {
    this.userService.changeProfilePicture(imageSrc, successCallback, errorCallback);
  }

  fetchProfilePicture(owner, successCallback, errorCallback) {
    this.userService.fetchProfilePicture(owner, successCallback, errorCallback);
  }

}
