/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class TeamsList {

  constructor(container) {
    this.utils = new Utils();
    this.controller = new TeamController();
    this.log = new Logger("TeamsList");

    this.container = $(container);
    $("body").append("<div id=\"team-list-entry-buffer\" class=\"d-none\">");
    let ref = this;
    this.utils.loadTemplate(container, "teams/team-list.html", function() {
      ref._setupSearch();
    })
  }

  addEntry(teamId, teamName, teamPrincipal, members) {
    let ref = this;
    this.utils.loadTemplate("#team-list-entry-buffer", "teams/team-list-entry.html", function() {
      let team = $("#team-list-entry-buffer .teams-list-entry:last-child");
      team.find(".team-name").text(teamName);
      team.find("input[name=id]").attr("value", teamId);
      members.forEach(function(member) {
        ref._addMemberToList(team, member);
      });
      team.find("button.add-member").click(function(e) {
        e.preventDefault();
        let modal = $("#add-team-member-modal");
        modal.find(".team-name").text(teamName);
        modal.find("input[name=team-id]").val(teamId);
        ref.controller.fetchUsers(teamPrincipal, function(users) {
          $("#users-list *").remove();
          users.forEach(function(user) {
            if("MASTER" != user.role && "ADMIN" != user.role && !ref._isMember(user, members)) {
              ref.utils.loadTemplate("#team-list-entry-buffer", "teams/user-entry.html", function() {
                let userContainer = $("#team-list-entry-buffer .user-entry:last-child");
                userContainer.find(".username").text(user.username);
                userContainer.find(".name").text(user.forename + " " + user.name);
                userContainer.find(".actions").append(
                  "<button class=\"btn btn-primary add-user\">" +
                  "<span class=\"fa fa-plus\"></span> " +
                  "<span class=\"src-only\">Hinzufügen</span>" +
                  "</button>"
                );
                userContainer.find("button.add-user").click(function(e) {
                  e.preventDefault();
                  ref.log.debug("Adding user " + user.id + " to team " + teamId);
                  ref.controller.addUserToTeam(user.id, teamId, function() {
                    ref._addMemberToList(team, user);
                    members.push(user);
                    userContainer.remove();
                  }, function() {});
                })
                userContainer.appendTo(modal.find("#users-list"));
                modal.find(".spinner-border").remove();
              });
            }
          });
          modal.find("input[name=user-search]").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#users-list").find(".user-entry").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
          });
          modal.modal('show');
        }, function() {})
      });
      team.appendTo("#teams-list");
    });
  }

  _setupSearch() {
    this.container.find("input[name=search]").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#teams-list").find(".teams-list-entry").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
      });
    });
  }

  _isMember(user, members) {
    let flag = false
    members.forEach(function(member) {
      flag = user.id == member.id;
    });
    return flag;
  }

  _addMemberToList(team, member) {
    team.find(".team-members").append(
      "<li class=\"list-group-item\" data-member=\"" + member.id + "\">" +
      member.forename + " " + member.name + " (" + member.username + ")</li>"
    );
  }

}
