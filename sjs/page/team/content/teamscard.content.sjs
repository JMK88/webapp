/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class TeamsCard {

  constructor(cardContainer, modalContainer) {
    this.utils = new Utils();
    this.controller = new TeamController();
    this.log = new Logger("TeamsCard");

    let templates = [];
    templates.push({container: cardContainer, name: "teams/manage-teams-card.html #manage-team-card"});
    templates.push({container: modalContainer, name: "teams/manage-teams-card.html #create-team-modal"});
    let ref = this;
    ref.utils.loadMultipleTemplates(templates, function() {
      ref._disablePrincipalSelection();
      ref._hookManageTeamsButtonListener();
      ref._hookTeamNameValidator();
      ref._hookSaveTeamListener();
    });
  }

  _disablePrincipalSelection() {
    let ref = this;
    this.controller.whoami(function(user) {
      ref.log.debug("User is: " + user.role);
      if("ADMIN" != user.role) {
        let principalSelection = $("#create-team-modal select[name=principal]");
        principalSelection.append(
          "<option value=\"" + user.principal + "\"></option>"
        );
        principalSelection.attr("disabled", "");
        principalSelection.hide();
        $("#create-team-modal label[for=principal]").hide();
      }
    });
  }

  _hookManageTeamsButtonListener() {
    $("#manage-teams").click(function(e) {
      e.preventDefault();
      new TeamView();
    })
  }

  _hookTeamNameValidator() {
    let input = $("#create-team-modal input[name=name]");
    let button = $("#create-team-modal button.save");
    let ref = this;
    input.on("keyup", function() {
      ref.log.debug("Validating team name input...");
      if(input.val().trim() !== "") {
        input.removeClass("is-invalid");
        input.addClass("is-valid");
        button.removeAttr("disabled");
      } else {
        input.removeClass("is-valid");
        input.addClass("is-invalid");
        button.attr("disabled", "");
      }
    });
  }

  _hookSaveTeamListener() {
    let name = $("#create-team-modal input[name=name]");
    let principal = $("#create-team-modal select[name=principal]");
    let button = $("#create-team-modal button.save");
    let ref = this;
    button.click(function(e) {
      e.preventDefault();
      ref.log.debug("Saving team " + name.val());
      ref.utils.addSpinner(button, true);
      button.attr("disabled", "");
      ref.controller.addTeam(principal.val(), name.val(), function(team) {
        ref.utils.removeSpinner(button, true);
        button.removeAttr("disabled");
        name.val("");
        $("#create-team-modal").modal('hide');
      }, function() {
        ref.utils.removeSpinner(button, true);
        button.removeAttr("disabled");
      });
    });
  }

}
