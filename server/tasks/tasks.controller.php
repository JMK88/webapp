<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

require_once "tasks/tasks.service.php";
require_once "logs/logger.php";
require_once "util/token.util.php";
require_once "exceptions/missing_parameters.exception.php";
require_once "exceptions/parameter_type.exception.php";

class TasksController {

  private Logger $logger;
  private TasksService $service;
  private TokenUtil $token_util;

  public function __construct() {
    $this->logger = new Logger("TasksController");
    $this->service = new TasksService();
    $this->token_util = new TokenUtil();
  }

  public function create_task() {
    $this->logger->debug("Add task called.");
    $user_id = $this->token_util->check_token();

    if(!isset($_POST["name"]))
      throw new MissingParametersException("Name of task missing.");
    if(!isset($_POST["xp"]))
      throw new MissingParametersException("XP for task missing.");

    $principal = null;
    if(isset($_POST["principal"])) {
      $principal = intval($_POST["principal"]);
      if(gettype($principal) != "integer") {
        throw new ParameterTypeException("Principal not given as number.");
      }
    }

    $name = $_POST["name"];
    $xp = intval($_POST["xp"]);

    if(gettype($xp) != "integer")
      throw new ParameterTypeException("XP not a number.");

    $description = null;
    if(isset($_POST["description"]))
      $description = $_POST["description"];

    $created = $this->service->create_task($user_id, $principal, $name, $description, $xp);

    $output_json = array(
      "id" => $created->get_id(),
      "principal" => $created->get_principal(),
      "name" => $created->get_name(),
      "description" => $created->get_description(),
      "xp" => $created->get_xp()
    );

    echo json_encode($output_json);
  }

  public function fetch_tasks() {
    $this->logger->debug("Fetch tasks called.");
    $user_id = $this->token_util->check_token();

    $principal = null;
    if(isset($_GET["principal"])) {
      $principal = intval($_GET["principal"]);
      if(gettype($principal) != "integer") {
        throw new ParameterTypeException("Principal not a number.");
      }
    }

    $tasks = $this->service->fetch_tasks($user_id, $principal);

    $tasks_json = array();

    foreach ($tasks as $task) {
      $task_json = array(
        "id" => $task->get_id(),
        "name" => $task->get_name(),
        "description" => $task->get_description(),
        "xp" => $task->get_xp(),
        "principal" => $task->get_principal()
      );
      $tasks_json[] = $task_json;
    }

    echo json_encode($tasks_json);
  }

  public function mark_done() {
    $this->logger->debug("Mark task as done called.");
    $user_id = $this->token_util->check_token();

    $boni = array();

    if(!isset($_POST["task"]))
      throw new MissingParametersException("ID of task missing.");
    if(!isset($_POST["asignees"]))
      throw new MissingParametersException("Task asignees missing.");
    if(!isset($_POST["archiveTask"]))
      throw new MissingParametersException("Flag missing if task should be archived.");

    $task = intval($_POST["task"]);
    if(gettype($task) != "integer")
      throw new ParameterTypeException("Task ID not a number");
    $asignees = $_POST["asignees"];
    if(gettype($asignees) != "array")
      throw new ParameterTypeException("Asignees not given as an array. Type: ".gettype($asignees));
    if(isset($_POST["boni"])) {
      $boni = $_POST["boni"];
      if(gettype($boni) != "array")
      throw new ParameterTypeException("Boni not given as an array. Type: ".gettype($boni));
    }
    $archive_task = $_POST["archiveTask"] == "true";

    $this->service->mark_done($user_id, $task, $asignees, $boni, $archive_task);
  }

  public function modify_task() {
    $this->logger->debug("Modify task called.");
    $user_id = $this->token_util->check_token();

    if(!isset($_POST["name"]))
      throw new MissingParametersException("Name of task missing.");
    if(!isset($_POST["xp"]))
      throw new MissingParametersException("XP for task missing.");
    if(!isset($_POST["id"]))
      throw new MissingParametersException("Task ID missing.");

    $name = $_POST["name"];
    $xp = intval($_POST["xp"]);
    $id = intval($_POST["id"]);

    if(gettype($xp) != "integer")
      throw new ParameterTypeException("XP not a number.");
    if(gettype($id) != "integer")
      throw new ParameterTypeException("ID not a number.");

    $description = null;
    if(isset($_POST["description"]))
      $description = $_POST["description"];

    $this->service->modify_task($user_id, $id, $name, $description, $xp);
  }

}

 ?>
