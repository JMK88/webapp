<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once 'token/token.dal.php';
include_once 'token/token.dto.php';
include_once 'logs/logger.php';
include_once 'exceptions/not_found.exception.php';

class TokenService {

  private $lower_case_chars = "abcdefghijklmnopqrstuvwxyz";
  private $upper_case_chars = "ABCDEFGHIJKLMNOPQRSTUVXYZ";
  private $numbers = "0123456789";

  private TokenDal $dal;
  private Logger $logger;

  public function __construct() {
    $this->dal = new TokenDal();
    $this->logger = new Logger("TokenService");
  }

  public function create_token(int $user_id) {
    $contents = $this->lower_case_chars.$this->upper_case_chars.$this->numbers;
    $token_string = "";
    for($i = 0; $i < 64; $i++) {
      $random = random_int(0,strlen($contents) - 1);
      $token_string = $token_string.substr($contents, $random, 1);
    }
    $expiry_date = new DateTime();
    $expiry_date->add(new DateInterval("P1D"));
    $token = new Token($token_string, $expiry_date, $user_id);
    $this->logger->debug("New token expiring ".$expiry_date->format("d.m.Y H:i,s")." issued: ".$token);
    try {
      $this->dal->store_token($token);
    } catch(NotStoredException $e) {
      $this->logger->debug("trying to generate token again.");
      return $this->create_token($user_id);
    }
    return $token;
  }

  public function fetch_token(string $token) {
    $fetched = $this->dal->fetch_token($token);
    $now = new DateTime();
    $diff = $now->diff($fetched->get_expiry_date());
    $num_diff = intval($diff->format("%R%h.%s"));
    if($num_diff > 0) {
      return $fetched;
    } else {
      try {
        $this->dal->delete_token($token);
      } catch(NotDeletedException $delex) {
        $this->logger->warn("Because token is not deleted, it's seen as invalid: ".$token);
      }
      $msg = "Token expired";
      $this->logger->error($msg);
      throw new NotFoundException($msg);
    }
  }

}

 ?>
