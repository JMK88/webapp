<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

require_once "team/team.dal.php";
require_once "logs/logger.php";
require_once "user/user.service.php";
require_once "exceptions/forbidden.exception.php";

class TeamService {

  private TeamDal $dal;
  private Logger $logger;
  private UserService $user_service;

  public function __construct() {
    $this->dal = new TeamDal();
    $this->logger = new Logger("TeamService");
    $this->user_service = new UserService();
  }

  public function add_team(int $user_id, int $principal, string $name) {
    $user = $this->user_service->fetch_user($user_id);
    if($user->get_role() !== "ADMIN" && $user->get_role() !== "MASTER")
      throw new ForbiddenException("Only admins and game masters are allowed to create teams.");
    return $this->dal->store_team($user_id, $principal, $name);
  }

  public function fetch_teams(int $user_id) {
    $user = $this->user_service->fetch_user($user_id);
    if($user->get_role() == "ADMIN")
      $teams = $this->dal->fetch_teams(null);
    elseif($user->get_role() == "MASTER")
      $teams = $this->dal->fetch_teams($user->get_principal());
    else
      $teams = $this->dal->fetch_assigned_teams($user_id);

    foreach ($teams as $team) {
      $member_numbers = $this->dal->fetch_team_members($team->get_id());
      $members = array();
      foreach ($member_numbers as $id) {
        $user = $this->user_service->fetch_user(intval($id));
        $members[] = $user;
      }
      $team->set_members($members);
    }
    return $teams;
  }

  public function assign_team(int $asigner, int $asignee, int $team) {
    $user = $this->user_service->fetch_user($asigner);
    if($user->get_role() != "ADMIN" && $user->get_role() != "MASTER")
      throw new ForbiddenException("Only admins and game masters are allowed to assign to teams");
    $this->dal->assign_team($asignee, $team);
  }

  public function fetch_team(int $user_id, int $team_id) {
    $user = $this->user_service->fetch_user($user_id);
    $team = $this->dal->fetch_team($team_id);
    $team->set_members($this->dal->fetch_team_members($team_id));
    $user_in_team = false;
    foreach ($team->get_members() as $member) {
      if($member == $user_id) {
        $user_in_team = true;
        break;
      }
    }
    if(!$user_in_team && $user->get_role() != "ADMIN" && $user->get_role() != "MASTER")
      throw new ForbiddenException("Only allowed to fetch teams you're assigned to unless you are an admin or game master.");
    return $team;
  }

}

 ?>
