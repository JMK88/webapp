<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once 'principal/principal.service.php';
include_once 'logs/logger.php';
include_once 'util/token.util.php';
include_once 'exceptions/forbidden.exception.php';
include_once 'exceptions/parameter_type.exception.php';

class PrincipalController {

  private PrincipalService $service;
  private Logger $logger;

  public function __construct() {
    $this->logger = new Logger("PrincipalController");
    $this->service = new PrincipalService();
    $this->token_util = new TokenUtil();
  }

  public function create_principal() {
    $this->logger->debug("Create principal called");

    $user_id = $this->token_util->check_token();

    if(!isset($_POST["name"])) {
      throw new MissingParametersException();
    }

    $name = $_POST["name"];

    if(gettype($name) != "string") {
      throw new ParameterTypeException();
    }

    $principal = $this->service->create_principal($user_id, $name);
    $principal_json = array(
      "id" => $principal->get_id(),
      "name" => $principal->get_name()
    );
    echo json_encode($principal_json);
  }

  public function fetch_principals() {
    $this->logger->debug("Fetch principals called");

    $user_id = $this->token_util->check_token();

    $principals = $this->service->fetch_principals($user_id);

    echo json_encode($principals);
  }

}

 ?>
