<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once 'user/user.service.php';
include_once 'exceptions/missing_parameters.exception.php';
include_once 'logs/logger.php';

class TokenUtil {

  private UserService $user_serivce;
  private Logger $logger;

  public function __construct() {
    $this->user_service = new UserService();
    $this->logger = new Logger("TokenUtil");
  }

  function check_token() {
    if(!isset($_SERVER["HTTP_TOKEN"])) {
      $msg = "Token not in header.";
      $this->logger->error($msg);
      throw new MissingParametersException($msg);
    }
    $token = $_SERVER["HTTP_TOKEN"];
    $token_object = $this->user_service->authenticate($token);
    return $token_object->get_user_id();
  }

}


 ?>
