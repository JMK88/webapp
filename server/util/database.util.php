<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once 'config.inc.php';
include_once 'exceptions/connection.exception.php';

function create_db_connection() {
  return TransactionManager::get_instance()->get_connection();
}

function fetch_id($mysqli, string $table, Logger $logger) {
  $query = "SELECT MAX(id) as max_id FROM ".TABLE_PREFIX.$table;
  $logger->debug("Executing query ".$query);
  $result = $mysqli->query($query);
  if($result == false) {
    $msg = "Not able to select maximum id from ".$table;
    $this->logger->error($msg);
    throw new NotFoundException($msg);
  }
  $row = $result->fetch_assoc();
  $max_id = intval($row["max_id"]);
  $new_id = $max_id + 1;
  return $new_id;
}

 ?>
