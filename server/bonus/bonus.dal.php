<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once "logs/logger.php";
include_once "util/database.util.php";
include_once "bonus/bonus.dto.php";

class BonusDal {

  private Logger $logger;

  public function __construct() {
    $this->logger = new Logger("BonusDal");
  }

  public function store_bonus(string $name, string $type, float $value, int $principal) {
    $mysqli = create_db_connection();
    $id = fetch_id($mysqli, "bonus", $this->logger);
    $query = "INSERT INTO ".TABLE_PREFIX."bonus ".
              "(id,principal,name,type,value) ".
              "VALUES ($id,$principal,".
              "'".$mysqli->real_escape_string($name)."',".
              "'".$mysqli->real_escape_string($type)."',".
              $value.
              ")";
    $this->logger->debug("Executing query: $query");
    $result = $mysqli->query($query);
    if(false == $result)
      throw new NotStoredException("Storing bonus aborded.");
    return new Bonus($id, $name, $type, $value, $principal);
  }

  public function fetch_boni(int $principal) {
    $mysqli = create_db_connection();
    $query = "SELECT * FROM ".TABLE_PREFIX."bonus ".
              "WHERE principal = $principal";
    $this->logger->debug("Executing query: $query");
    $result = $mysqli->query($query);
    if(false == $result)
      throw new NotFoundException("Could not fetch boni");
    $boni = array();
    while($row = $result->fetch_assoc()) {
      $boni[] = new Bonus(intval($row["id"]),
                $row["name"], $row["type"], doubleval($row["value"]),
                intval($row["principal"]));
    }
    return $boni;
  }

  public function assign_bonus(int $task, int $bonus) {
    $mysqli = create_db_connection();
    $date = date("Y-m-d H:i:s");
    $query = "INSERT INTO ".TABLE_PREFIX."gained_bonus ".
              "(task,bonus,gained_date) ".
              "VALUES ($task, $bonus,".
              "'".$mysqli->real_escape_string($date)."'".
              ")";
    $this->logger->debug("Executing query: $query");
    $result = $mysqli->query($query);
    if(false == $result)
      throw new NotStoredException("Bonus not assigned.");
  }

}


 ?>
