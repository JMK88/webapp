<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once "logs/logger.php";
include_once "ranking/ranking.service.php";
include_once "util/token.util.php";
include_once "exceptions/missing_parameters.exception.php";

class RankingController {

  private Logger $logger;
  private RankingService $service;

  public function __construct() {
    $this->logger = new Logger("RankingController");
    $this->service = new RankingService();
    $this->token_util = new TokenUtil();
  }

  public function get_ranking() {
    $user_id = $this->token_util->check_token();

    if(!isset($_GET["team"]))
      throw new MissingParametersException("Team id not set.");

    $team = intval($_GET["team"]);
    if(gettype($team) != "integer")
      throw new ParameterTypeException("Team not given as ID");

    $ranking = $this->service->create_ranking($user_id, $team);

    $ret_val = json_encode($ranking);
    $this->logger->debug("Returning $ret_val");
    echo $ret_val;
  }

}

 ?>
