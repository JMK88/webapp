DROP TABLE IF EXISTS gained_bonus;
DROP TABLE IF EXISTS done_tasks;
DROP TABLE IF EXISTS bonus;
DROP TABLE IF EXISTS task;
DROP TABLE IF EXISTS team_members;
DROP TABLE IF EXISTS team;
DROP TABLE IF EXISTS token;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS principal;

CREATE TABLE principal (
  id INT,
  name VARCHAR(255),
  PRIMARY KEY (id)
);

CREATE TABLE user (
  id INT,
  principal INT NULL,
  gender CHAR(1),
  date_of_birth DATE,
  creation_date TIMESTAMP,
  username VARCHAR(255) UNIQUE,
  password VARCHAR(255) NOT NULL,
  name VARCHAR(255),
  forename VARCHAR(255),
  role VARCHAR(255) NOT NULL,
  picture MEDIUMTEXT,
  PRIMARY KEY (id),
  FOREIGN KEY (principal) REFERENCES principal(id)
);

CREATE TABLE token (
  token CHAR(64),
  user INT NOT NULL,
  expiry_date TIMESTAMP,
  PRIMARY KEY (token),
  FOREIGN KEY (user) REFERENCES user(id)
);

CREATE TABLE team (
  id INT,
  principal INT,
  name VARCHAR(255),
  PRIMARY KEY (id),
  FOREIGN KEY (principal) REFERENCES principal(id)
);

CREATE TABLE team_members (
  user INT,
  team INT,
  PRIMARY KEY (user, team),
  FOREIGN KEY (user) REFERENCES user(id),
  FOREIGN KEY (team) REFERENCES team(id)
);

CREATE TABLE task (
  id INT,
  xp INT NOT NULL,
  principal INT NOT NULL,
  name VARCHAR(255) NOT NULL,
  description VARCHAR(1024),
  archived BOOLEAN NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (principal) REFERENCES principal(id)
);

CREATE TABLE done_tasks (
  id INT PRIMARY KEY,
  task INT NOT NULL,
  user INT NOT NULL,
  done_date TIMESTAMP NOT NULL,
  FOREIGN KEY (task) REFERENCES task(id),
  FOREIGN KEY (user) REFERENCES user(id),
  UNIQUE (task, user, done_date)
);

CREATE TABLE bonus (
  id INT,
  principal INT NOT NULL,
  value DECIMAL(9,2) NOT NULL,
  name VARCHAR(255) NOT NULL,
  type VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (principal) REFERENCES principal(id)
);

CREATE TABLE gained_bonus (
  task INT NOT NULL,
  bonus INT NOT NULL,
  gained_date TIMESTAMP NOT NULL,
  PRIMARY KEY (task, bonus),
  FOREIGN KEY (task) REFERENCES done_tasks(id),
  FOREIGN KEY (bonus) REFERENCES bonus(id)
);
