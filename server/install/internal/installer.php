<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once 'config.inc.php';
include_once 'exceptions/connection.exception.php';
include_once 'logs/logger.php';
include_once 'exceptions/not_stored.exception.php';
include_once 'util/transactionmanager.util.php';

class Installer {

  private Logger $logger;
  private bool $successful = false;

  public function __construct() {
    $this->logger = new Logger("Installer");
  }

  public function install() {
    $tm = TransactionManager::get_instance();
    $mysqli = $tm->get_connection();

    $tm->open_transaction(false);

    // add the table prefix to all tables
    $sql = file_get_contents("install/internal/database.sql");
    $replaced_drops = str_replace("DROP TABLE IF EXISTS ", "DROP TABLE IF EXISTS ".TABLE_PREFIX, $sql);
    $replaced_create_tables = str_replace("CREATE TABLE ", "CREATE TABLE ".TABLE_PREFIX, $replaced_drops);
    $replaced_references = str_replace("REFERENCES ", "REFERENCES ".TABLE_PREFIX, $replaced_create_tables);
    $final_sql = str_replace("INSERT INTO ", "INSERT INTO ".TABLE_PREFIX, $replaced_references);
    // get every line of file to excecute it seperatly
    $lines = explode(";", $final_sql);
    foreach ($lines as $line) {
      $query = trim($line);
      if("" === $query) continue;
      $this->logger->debug("Executing query ".$query);
      $result = $mysqli->query($query);
      if(false == $result) {
        $tm->rollback();
        throw new NotStoredException("Installation aborded. Query failed (".$query.")");
      }
    }
    // create first user
    $password = password_hash("TheAdmin", PASSWORD_BCRYPT);
    $query = "INSERT INTO ".TABLE_PREFIX."user (id,username,password,role)".
                    " VALUES (1,'admin','".$password."','ADMIN')";
    $this->logger->debug("Creating user: ".$query);
    $result = $mysqli->query($query);
    if(false == $result) {
      $tm->rollback();
      throw new NotStoredException("Installation aborded. Query failed (".$query.")");
    }
    $this->successful = true;
    $tm->commit();
    $this->logger->info("Installation complete");
  }

  function is_successful() {
    return $this->successful;
  }

}

?>
